package psp.jesus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ejercicio3 {
    public static void main(String[] args) {
        String prodrama = "java -jar ../Minusculas/out/artifacts/Minusculas_jar/Minusculas.jar";

        List<String> argslist = new ArrayList<>(Arrays.asList(prodrama.split(" ")));

        ProcessBuilder pb = new ProcessBuilder(argslist);

        pb.inheritIO();

        /**
         * Se debia haber realizado mediante inputStream - OtuputStream
         * porque se especifica en el ejercicio
         */

        try {
            Process process = pb.start();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
