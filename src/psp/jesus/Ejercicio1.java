package psp.jesus;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {

    public static void main(String[] args){

        try{

            /**
             * Ha faltado guardar el archivo y comprobar que la salida del proceso fuera correcta
             */
            File f = new File("output.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));

            ProcessBuilder pb = new ProcessBuilder(args);
            Process comandos = pb.start();

            InputStream is = comandos.getInputStream();

            Scanner sc = new Scanner(is);

            bw.write(sc.nextLine());

            if (!comandos.waitFor(2, TimeUnit.SECONDS)){
                System.out.println("tiempo agotado");
            }

            while (sc.hasNext()){
                System.out.println(sc.nextLine());
            }
        }catch (IOException | InterruptedException e){
            e.printStackTrace();
        }

    }
}
